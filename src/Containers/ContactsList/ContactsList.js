import React, {Component} from 'react';
import {connect} from "react-redux";
import {getContacts, hideModalHandler, showModalHandler} from "../../store/actions/contacts";
import Contact from "../../Components/Contacts/Contact/Contact";
import Modal from "../../Components/UI/Modal/Modal";
import MoreInfoContact from "../../Components/Contacts/MoreInfoContact/MoreInfoContact";

class ContactsList extends Component {

  state = {
    keyContact: '',
  };

  componentDidMount() {
    this.props.getContacts();
  };

  moreInfoContact = key => {
    this.setState({keyContact: key});
    this.props.showModalHandler();
  };

  editContact = () => this.props.history.push(`/edit/${this.state.keyContact}`);

  render() {
    let modal;
    if (this.props.showModal) {
      modal = (
        <Modal show={this.props.showModal} closed={this.props.hideModalHandler}>
          <MoreInfoContact edit={this.editContact}
                           keyContact={this.state.keyContact}/>
        </Modal>
      );
    } else {
      modal = null;
    }
    return(
      <div className='Contacts-List'>
        {modal}
        {this.props.contacts
          ? Object.keys(this.props.contacts)
          .map(key => (
            <Contact key={key}
                     clicked={() => this.moreInfoContact(key)}
                     name={this.props.contacts[key].name}
                     photo={this.props.contacts[key].photo}
              />))
          : null
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    showModal: state.showModal,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContacts()),
    showModalHandler: () => dispatch(showModalHandler()),
    hideModalHandler: () => dispatch(hideModalHandler()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);