import React, { Component } from 'react';
import './App.css';
import Layout from "./Components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import ContactsList from "./Containers/ContactsList/ContactsList";
import AddNewContact from "./Components/Contacts/AddNewContact/AddNewContact";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/' exact component={ContactsList}/>
          <Route path='/add-new-contact' exact component={AddNewContact}/>
          <Route path='/edit/:id' exact
                 render={(props) => <AddNewContact {...props} edit />}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
