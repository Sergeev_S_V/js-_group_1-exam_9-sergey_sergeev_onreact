import React, {Component} from 'react';
import Button from "../../UI/Button/Button";
import {connect} from "react-redux";
import {editContact, getDetailsContact, hideModalHandler, saveContact} from "../../../store/actions/contacts";
import './AddNewContact.css';

class AddNewContact extends Component {

  state = {
    name: '',
    phone: '',
    email: '',
    photo: '',
  };

  componentDidMount() {
    this.props.getDetailsContact(this.props.match.params.id);
    if (this.props.edit) {
      this.setState({
        name: this.props.detailsContact.name,
        phone: this.props.detailsContact.phone,
        email: this.props.detailsContact.email,
        photo: this.props.detailsContact.photo,
      })
    }
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  saveContactHandler = event => {
    event.preventDefault();
    const contact = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      photo: this.state.photo,
    };
    if (this.props.edit) {
      this.props.editContact(contact, this.props.history, this.props.match.params.id);
    } else {
      this.props.saveContact(contact, this.props.history);
    }
  };

  backToContacts = () => {
    this.props.hideModalHandler();
    this.props.history.push('/');
  };

  render() {
    let previewForm;
    if (this.state.photo) {
      previewForm = (
        <div className='Form-preview'>
          <span>Photo preview</span>
          <img src={this.state.photo} alt=""/>
        </div>
      );
    }
    return(
      <div className='Add-New-Contact'>
        <form action="">
          <div className='Form-name'>
            <label htmlFor="">Name</label>
            <input type="text"
                   name='name'
                   value={this.state.name}
                   onChange={this.inputChangeHandler}
                   placeholder='Your name'/>
          </div>
          <div className='Form-phone'>
            <label htmlFor="">Phone</label>
            <input type="text"
                   name='phone'
                   onChange={this.inputChangeHandler}
                   value={this.state.phone}
                   placeholder='Your phone'/>
          </div>
          <div className='Form-email'>
            <label htmlFor="">Email</label>
            <input type="text"
                   name='email'
                   value={this.state.email}
                   onChange={this.inputChangeHandler}
                   placeholder='Your email'/>
          </div>
          <div className='Form-photo'>
            <label htmlFor="">Photo</label>
            <input type="text"
                   name='photo'
                   value={this.state.photo}
                   onChange={this.inputChangeHandler}
                   placeholder='Your photo'/>
          </div>
          {previewForm}
          <Button clicked={this.saveContactHandler}>Save</Button>
          <Button clicked={this.backToContacts}>Back to contacts</Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    detailsContact: state.detailsContact,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    saveContact: (contact, history) => dispatch(saveContact(contact, history)),
    editContact: (contact, history, key) => dispatch(editContact(contact, history, key)),
    hideModalHandler: () => dispatch(hideModalHandler()),
    getDetailsContact: keyContact => dispatch(getDetailsContact(keyContact)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact);