import React from 'react';
import './Contact.css';

const Contact = props => (
  <div className='Contact' onClick={props.clicked}>
    <img src={props.photo} alt=""/>
    <span className='Name'>{props.name}</span>
  </div>
);

export default Contact;