import React, {Component} from 'react';
import {connect} from "react-redux";
import Button from "../../UI/Button/Button";
import './MoreInfoContact.css';
import {deleteContact, getDetailsContact, hideModalHandler} from "../../../store/actions/contacts";

class MoreInfoContact extends Component {

  componentDidMount() {
    this.props.getDetailsContact(this.props.keyContact)
  };

  render() {
    if (this.props.detailsContact) {
      return(
        <div className='More-Info-Contact'>
          <span className='Close' onClick={this.props.hideModalHandler}>X</span>
          <img className='Contact-Photo' src={this.props.detailsContact.photo} alt=""/>
          <div className='Contacts'>
            <span className='Contact-Name'>{this.props.detailsContact.name}</span>
            <span className='Contact-Phone'>{this.props.detailsContact.phone}</span>
            <span className='Contact-Email'>{this.props.detailsContact.email}</span>
            <Button clicked={this.props.edit}>Edit</Button>
            <Button clicked={() => this.props.deleteContact(this.props.keyContact)}>Delete</Button>
          </div>
        </div>
      );
    } else {
      return <div>No selected contact</div>
    }
  }
}

const mapStateToProps = state => {
  return {
    detailsContact: state.detailsContact,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getDetailsContact: keyContact => dispatch(getDetailsContact(keyContact)),
    deleteContact: keyContact => dispatch(deleteContact(keyContact)),
    hideModalHandler: () => dispatch(hideModalHandler()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MoreInfoContact);