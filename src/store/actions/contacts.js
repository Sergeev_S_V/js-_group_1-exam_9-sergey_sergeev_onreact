import * as actionTypes from './actionTypes';
import axios from '../../axios-exam';

export const saveContact = (contact, history) => dispatch => {
  axios.post(`/contacts.json`, contact)
    .then(resp => {
      if (resp) {
        dispatch(getContacts());
        history.push('/');
        dispatch(hideModalHandler());
      }
    })
};

export const editContact = (contact, history, key) => dispatch => {
  axios.patch(`/contacts/${key}.json`, contact)
    .then(resp => {
      if (resp) {
        dispatch(getContacts());
        history.push('/');
        dispatch(hideModalHandler());
      }
    })
};

export const successResponseGetContacts = contacts => {
  return {type: actionTypes.SUCCESS_RESPONSE_GET_CONTACTS, contacts};
};

export const getContacts = () => dispatch => {
  axios.get(`/contacts.json`)
    .then(contacts => contacts
      ? dispatch(successResponseGetContacts(contacts.data))
      : null
    )
};

export const successResponseDetailsContact = contact => {
  return {type: actionTypes.SUCCESS_RESPONSE_DETAILS_CONTACT, contact}
};

export const getDetailsContact = keyContact => dispatch => {
  axios.get(`/contacts/${keyContact}.json`)
    .then(contact => contact
      ? dispatch(successResponseDetailsContact(contact.data))
      : null
    )
};

export const deleteContact = keyContact => dispatch => {
  axios.delete(`/contacts/${keyContact}.json`)
    .then(resp => {
      if (resp) {
        dispatch(getContacts());
        dispatch(hideModalHandler());
      }
    })
};

export const showModalHandler = () => {
  return {type: actionTypes.SHOW_MODAL};
};

export const hideModalHandler = () => {
  return {type: actionTypes.HIDE_MODAL};
};
