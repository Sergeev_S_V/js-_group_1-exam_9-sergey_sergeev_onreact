import * as actionTypes from '../actions/actionTypes';

const initialState = {
  contacts: {},
  detailsContact: {},
  showModal: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SUCCESS_RESPONSE_GET_CONTACTS:
      return {...state, contacts: action.contacts};
    case actionTypes.SUCCESS_RESPONSE_DETAILS_CONTACT:
      return {...state, detailsContact: action.contact};
    case actionTypes.SHOW_MODAL:
      return {...state, showModal: true};
    case actionTypes.HIDE_MODAL:
      return {...state, showModal: false};
    default:
      return state;
  }
};

export default reducer;