import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import contacts from "./store/reducers/contacts";
// import reducer2 from "./store/reducers/reducer2";

// const rootReducer = combineReducers({
//   a: reducer1,
//   b: reducer2,
// });

const store = createStore(contacts, applyMiddleware(thunkMiddleware));
const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);


ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
