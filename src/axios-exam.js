import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://js-exam-98426.firebaseio.com',
});

export default instance;